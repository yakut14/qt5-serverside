#include "solver.h"

#include <QRegExp>

Solver::Solver()
{
}

int Solver::calculate( const QString& txt, const QString& word )
{
    QRegExp expres( "\\b("+ word +")\\b" );

    int pos = 0;
    int count = 0;
    while( (pos = expres.indexIn( txt, pos ) ) != -1 )
    {
        count++;
        pos += expres.matchedLength();
    }

    return count;
}

