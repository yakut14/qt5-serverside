#ifndef SOLVER_H
#define SOLVER_H

#if defined ( _WINDOWS )
#define EXPORT_FUNCTION __declspec( dllexport )
#else
#define EXPORT_FUNCTION
#endif

#include <QString>

class Solver
{
public:
    EXPORT_FUNCTION Solver();
    EXPORT_FUNCTION int calculate( const QString& txt, const QString& word );
};

#endif // SOLVER_H
