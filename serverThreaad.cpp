#include "serverThreaad.h"
#include "solver.h"

#include <QTcpSocket>
#include <QDataStream>
#include <QFile>
#include <QUuid>


SocketThread::SocketThread( int socketDescriptor, QObject *parent ) : QThread( parent )
{
    m_desc = socketDescriptor;
    m_clientConnection = NULL;
}

SocketThread::~SocketThread()
{
    if ( m_clientConnection )
        delete m_clientConnection;
}

void SocketThread::run()
{
    m_clientConnection = new QTcpSocket();
    if ( !m_clientConnection->setSocketDescriptor( m_desc ) )
        return;

    //connect( m_clientConnection, SIGNAL( readyRead() ), this, SLOT( calculate() ), Qt::DirectConnection ); одной строкой
    connect( m_clientConnection, SIGNAL( readyRead() ), this, SLOT( readMsg() ), Qt::DirectConnection );
    connect( m_clientConnection, SIGNAL( disconnected() ), this, SLOT( onDisconnected() ), Qt::DirectConnection );

    exec();
}


//!Ключевое слово дописано в конец сообщения
void SocketThread::calculate()
{
    QString txt = m_clientConnection->readAll();
    QString word = txt.right( txt.count() - txt.lastIndexOf( " #" ) - 2 );

    Solver s;
    int count = s.calculate( txt, word );
    count -= 1; //само ключевое слово

    sendMessage( QString::number( count ) );
}

void SocketThread::sendMessage( const QString& msg )
{
    QByteArray block( msg.toStdString().c_str() );
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    m_clientConnection->write( block );
    m_clientConnection->flush();
    m_clientConnection->waitForBytesWritten(3000);
    m_clientConnection->disconnectFromHost();

    quit();
}

void SocketThread::readMsg()
{
    QDataStream out( m_clientConnection );
    out.setVersion( QDataStream::Qt_4_0 );

    if( !m_inputData.m_inputF )
    {
        QString txt;
        out >> txt;
        m_inputData.m_inputFileSize = txt.toInt();
        m_inputData.m_inputF = new QFile( QUuid::createUuid().toString() + ".txt" );
        fprintf( stderr,  m_inputData.m_inputF->fileName().toStdString().c_str() );
        if( !m_inputData.m_inputF->open( QIODevice::WriteOnly ) )
            fprintf( stderr, "Нельзя записать файл" );

        QString word;
        out >> word;

        m_inputData.m_word = word;
    }

    QByteArray txt = m_clientConnection->readAll();
    if( m_inputData.m_inputFileSize != m_inputData.m_inputF->size() )
    {
        m_inputData.m_inputF->write( txt.toStdString().c_str() );
    }

    if( m_inputData.m_inputFileSize == m_inputData.m_inputF->size() )
    {
        m_inputData.m_inputF->close();
        if( m_inputData.m_inputF->open( QIODevice::ReadOnly ) )
        {
            Solver s;
            int res = s.calculate( m_inputData.m_inputF->readAll(), m_inputData.m_word  );
            sendMessage( QString::number( res ) );
        }
    }
}

void SocketThread::onDisconnected()
{
    clearInputData();
}

void SocketThread::clearInputData()
{
    m_inputData.m_inputFileSize = 0;
    if( m_inputData.m_inputF )
    {
        if( m_inputData.m_inputF->isOpen() )
            m_inputData.m_inputF->close();

        delete m_inputData.m_inputF;
        m_inputData.m_inputF = NULL;
    }
}

