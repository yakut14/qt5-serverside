#include "server.h"
#include "serverThreaad.h"


#include <QTcpSocket>
#include <QApplication>
#include <QDataStream>
#include <QRegExp>
#include <QFile>
#include <QUuid>


void TcpServer::incomingConnection( qintptr socketDescriptor )
{
    SocketThread *thread = new SocketThread( socketDescriptor, this );
    connect( thread, SIGNAL( finished() ), thread, SLOT( deleteLater() ) );
    thread->start();
}

Server::Server() :  QObject()
{
    //!Single
    //m_tcpServer = new QTcpServer( this );
    //connect( m_tcpServer, SIGNAL( newConnection() ), this, SLOT( newConnection() ) );
    //!Multiple
    //m_tcpServer = new TcpServer( this );
    //!Multiple Map
    m_tcpServer = new QTcpServer( this );
    connect( m_tcpServer, SIGNAL( newConnection() ), this, SLOT( newConnectionMap() ) );

    sessionOpened();
}

void Server::newConnectionMap()
{
    QTcpSocket* clientConnection = m_tcpServer->nextPendingConnection();
    m_data[ clientConnection ] = InputData();
    connect( clientConnection, SIGNAL( readyRead() ), this, SLOT( readMsg() ) );
    connect( clientConnection, SIGNAL( disconnected() ), this, SLOT( onDisconnected() ));
}

void Server::newConnection()
{
    clearInputData( &m_inputData );
    m_clientConnection = m_tcpServer->nextPendingConnection();

    //connect( m_clientConnection, SIGNAL( readyRead() ), this, SLOT( calculate() ) );  //одной строкрй
    connect( m_clientConnection, SIGNAL( readyRead() ), this, SLOT( readMsg() ) );
    connect( m_clientConnection, SIGNAL( disconnected() ), this, SLOT( onDisconnected() ) );

}

void Server::onDisconnected()
{
    QTcpSocket* clientConnection = dynamic_cast< QTcpSocket* >( sender() );
    if( !clientConnection )
        return;

    if( m_data.contains( clientConnection ) )
    {
        clearInputData( &m_data[ clientConnection ] );
        m_data.remove( clientConnection );
    }
    else
        clearInputData( &m_inputData );
}

void Server::clearInputData( InputData* inputData )
{
    inputData->m_inputFileSize = 0;
    if( inputData->m_inputF )
    {
        if( inputData->m_inputF->isOpen() )
            inputData->m_inputF->close();

        delete inputData->m_inputF;
        inputData->m_inputF = NULL;
    }
}

Server::~Server()
{

}

void Server::sessionOpened()
{
    if (!m_tcpServer->listen( QHostAddress::Any, 44470 ) )
    {
        fprintf( stderr, "Unable to start the server: %d.", m_tcpServer->errorString() );
        QApplication::closeAllWindows();
        return;
    }

    fprintf( stderr, "Port is: %d", m_tcpServer->serverPort() );
}


int Server::calculate( const QString& txt, const QString& word )
{
    QRegExp expres( "\\b("+ word +")\\b" );

    int pos = 0;
    int count = 0;
    while( (pos = expres.indexIn( txt, pos ) ) != -1 )
    {
        count++;
        pos += expres.matchedLength();
    }

    return count;
}

//!Ключевое слово дописано в конец сообщения
void Server::calculate()
{
    QString txt = m_clientConnection->readAll();
    QString word = txt.right( txt.count() - txt.lastIndexOf( " #" ) - 2 );

    int count = calculate( txt, word );
    count -= 1; //само ключевое слово

    sendMessage( m_clientConnection, QString::number( count ) );
}

void Server::sendMessage( QTcpSocket* clientConnection, const QString& msg )
{
    QByteArray block( msg.toStdString().c_str() );
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    connect(clientConnection, &QAbstractSocket::disconnected,
            clientConnection, &QObject::deleteLater);

    clientConnection->write( block );
    clientConnection->flush();
    clientConnection->waitForBytesWritten(3000);
    clientConnection->disconnectFromHost();
}

void Server::readMsg( QTcpSocket* clientConnection, InputData* inputData )
{
    QDataStream out( clientConnection );
    out.setVersion( QDataStream::Qt_4_0 );

    if( !inputData->m_inputF )
    {
        QString txt;
        out >> txt;
        inputData->m_inputFileSize = txt.toInt();
        inputData->m_inputF = new QFile( QUuid::createUuid().toString() + ".txt" );
        fprintf( stderr,  inputData->m_inputF->fileName().toStdString().c_str() );
        if( !inputData->m_inputF->open( QIODevice::WriteOnly ) )
            fprintf( stderr, "Нельзя записать файл" );

        QString word;
        out >> word;

        inputData->m_word = word;
    }

    QByteArray txt = clientConnection->readAll();
    if( inputData->m_inputFileSize != inputData->m_inputF->size() )
    {
        inputData->m_inputF->write( txt.toStdString().c_str() );
    }

    if( inputData->m_inputFileSize == inputData->m_inputF->size() )
    {
        inputData->m_inputF->close();
        if( inputData->m_inputF->open( QIODevice::ReadOnly ) )
        {
            int res = calculate( inputData->m_inputF->readAll(), inputData->m_word  );
            inputData->m_inputF->close();
            sendMessage( clientConnection, QString::number( res ) );
        }
    }
}

void Server::readMsg()
{
    QTcpSocket* clientConnection = dynamic_cast< QTcpSocket* >( sender() );
    if( !clientConnection )
        return;

    InputData* inputData;
    if( m_data.contains( clientConnection ) )
        inputData = &m_data[ clientConnection ];
    else
        inputData = &m_inputData;

    readMsg( clientConnection, inputData );
}
