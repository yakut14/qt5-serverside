#ifndef SERVER_H
#define SERVER_H

#include "interfaces.h"

#include <QObject>
#include <QAbstractSocket>
#include <QTcpServer>

class QTcpSocket;
class QNetworkSession;
class QFile;



class TcpServer : public QTcpServer
{
    Q_OBJECT
public:
    TcpServer( QObject* p = NULL ) : QTcpServer( p ){}

protected:
    virtual void incomingConnection( qintptr socketDescriptor ) override;
};

class Server : public QObject
{
    Q_OBJECT

public:
    Server();
    ~Server();

protected:
    int calculate( const QString&, const QString& );
    void clearInputData( InputData *inputData );
    void sendMessage( QTcpSocket*, const QString& msg );

    void readMsg( QTcpSocket* , InputData *inputData );


protected slots:
    void sessionOpened();
    void calculate();
    void newConnection();
    void newConnectionMap();
    void readMsg();
    void onDisconnected();

protected:
    QTcpServer* m_tcpServer;
    //!Map
    QMap< QTcpSocket*, InputData > m_data;
    //!Single
    QTcpSocket* m_clientConnection;
    //!Файловый режим
    InputData m_inputData;
};

#endif // SERVER_H
