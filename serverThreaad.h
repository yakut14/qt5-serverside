#ifndef SERVER_TRHREAD_H
#define SERVER_TRHREAD_H

#include <QThread>
#include "interfaces.h"

class QTcpSocket;
class QFile;

class SocketThread : public QThread
{
    Q_OBJECT

public:
    SocketThread( int socketDescriptor, QObject *parent );
    ~SocketThread();

    void run();

protected:
    void sendMessage( const QString& msg );
    void clearInputData();

protected slots:
    void calculate();
    void readMsg();
    void onDisconnected();

protected:
    int m_desc;
    QTcpSocket* m_clientConnection;

    //!Файловый режим
    InputData m_inputData;
};

#endif // SERVER_TRHREAD_H
