#include <gtest/gtest.h>
#include <iostream>

#include "solver.h"

#include <QString>

TEST(SolverTest, test3)
{
    Solver s;
    QString txt = "11 1";
    QString word = "1";

    ASSERT_EQ( s.calculate( txt, word ), 1 );
}


int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
