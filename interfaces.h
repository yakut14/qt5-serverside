#ifndef SERVER_INTERFACE_H
#define SERVER_INTERFACE_H

#include <QString>

class QFile;

struct InputData
{
    InputData()
    {
        m_inputFileSize = 0;
        m_inputF = NULL;
    }
    int m_inputFileSize;
    QString m_word;
    QFile* m_inputF;
};

#endif // SERVER_INTERFACE_H
